import React from "react";
import { NavigationContainer } from "@react-navigation/native";

import MainTabScreen from "./app/screens/tabsNavigation/MainTab";

function App() {
  return (
    <NavigationContainer>
      <MainTabScreen/>
    </NavigationContainer>
  );
}

export default App;
