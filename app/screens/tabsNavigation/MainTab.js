import React from "react";

import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";

import TransferenceFirst from "../../components/TransferenceFirst";
import TransferenceSecond from "../../components/TransferenceSecond";
import TransferenceThird from "../../components/TransferenceThird";
import { Image } from "react-native";
import StackScreen from "./StackScreen";

const Tab = createMaterialBottomTabNavigator();

function MainTabScreen() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#fff"
      shifting={true}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: "Home",
          tabBarColor: "#1f65ff",
          tabBarIcon: ({ color }) => (
            <Image
              source={require("../../assets/home.png")}
              resizeMode="contain"
              style={{ width: 20, height: 20, tintColor: color }} />
          ),
        }}
      />
      <Tab.Screen
        name="Transference"
        component={TransferenceStackScreen}
        options={{
          tabBarLabel: "Transference",
          tabBarColor: "#d02860",
          tabBarIcon: ({ color }) => (
            <Image
              source={require("../../assets/money.png")}
              resizeMode="contain"
              style={{ width: 20, height: 20, tintColor: color }} />
          ),
        }}
      />
      <Tab.Screen
        name="Details"
        component={DetailsStackScreen}
        options={{
          tabBarLabel: "Details",
          tabBarColor: "#34495E",
          tabBarIcon: ({ color }) => (
            <Image
              source={require("../../assets/campana.png")}
              resizeMode="contain"
              style={{ width: 20, height: 20, tintColor: color }} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default MainTabScreen;

const HomeStackScreen = () => (
  <StackScreen bgColor="#1f65ff" name="Home" component={TransferenceFirst} title="Home" />
);

const TransferenceStackScreen = () => (
  <StackScreen bgColor="#d02860" name="Transference" component={TransferenceSecond} title="Transference" />
);

const DetailsStackScreen = () => (
  <StackScreen bgColor="#34495E" name="Details" component={TransferenceThird} title="Details" />
);
