import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

function StackScreen(props) {
  const { bgColor, name, component, title } = props
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: {
        backgroundColor: bgColor,
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
    }}>
      <Stack.Screen name={name} component={component} options={{
        title: title,
      }} />
    </Stack.Navigator>
  );
}

export default StackScreen;
