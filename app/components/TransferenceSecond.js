import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Input from "./Input";

function TransferenceSecond({ navigation, route }) {
  // const {rootAccount, destinationAccount, amountText, referenceText, dateDisplay, sendEmail} = route.params

  return (
    <View style={styles.container}>
      <Input text="Cuenta Origen" value={route.params?.rootAccount} editable={false} />
      <Input text="Cuenta Destino" value={route.params?.destinationAccount} editable={false} />
      <Input text="Importe" value={`S/ ${route.params?.amountText}`} editable={false} />
      <Input text="Referencia" value={route.params?.referenceText} editable={false} />
      <Input text="Fecha" value={route.params?.dateDisplay} editable={false} />
      <Input text="Correo" value={route.params?.sendEmail} editable={false} />
      <View style={styles.containerBtn}>
        <TouchableOpacity style={styles.btnOk} onPress={() => navigation.navigate("Home")}>
          <Text style={styles.labelBtn}>Volver</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnOk} onPress={() => navigation.navigate("Details")}>
          <Text style={styles.labelBtn}>Confirmar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default TransferenceSecond;

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingRight: 25,
  },

  containerBtn: {
    margin: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },

  labelBtn: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
  },

  btnOk: {
    width: 150,
    height: 50,
    backgroundColor: "#d02860",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
});
