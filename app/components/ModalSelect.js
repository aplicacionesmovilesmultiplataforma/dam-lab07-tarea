import React from "react";
import { View, TouchableOpacity, StyleSheet, Text, ScrollView } from "react-native";

const OPTIONS = ["Seleccione una cuenta", "0000000000158950", "0000000000785036", "0000000000203996",
  "0000000000689503", "0000000000750369", "0000000000807930", "0000000000667850"];

function ModalSelect(props) {
  const { changeModalVisibility, setData, marginTop } = props;

  const onPressItem = (option) => {
    changeModalVisibility(false);
    setData(option);
  };
  const option = OPTIONS.map((item, index) => {
    return (
      <TouchableOpacity key={index} onPress={() => onPressItem(item)}>
        <Text style={styles.labelOption}>
          {item}
        </Text>
      </TouchableOpacity>
    );
  });
  return (
    <TouchableOpacity style={{ flex: 1, alignItems: "center", marginTop: marginTop }}
                      onPress={() => changeModalVisibility(false)}>
      <View style={[styles.modal, { width: 342, height: 265 }]}>
        <ScrollView>
          {option}
        </ScrollView>
      </View>
    </TouchableOpacity>
  );
}

export default ModalSelect;

const styles = StyleSheet.create({

  modal: {
    backgroundColor: "white",
    borderRadius: 10,
  },

  labelOption: {
    margin: 20,
    fontSize: 20,
    fontWeight: "bold",
  },
});
