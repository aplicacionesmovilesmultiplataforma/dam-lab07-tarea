import React, { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Modal, Image } from "react-native";
import ModalSelect from "./ModalSelect";

function Select(props) {
  const { text, chooseData, setData, marginTop } = props;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const changeModalVisibility = (bool) => setIsModalVisible(bool);

  return (
    <View style={styles.containerInput}>
      <Text style={styles.labelStyle}>{text}</Text>
      <TouchableOpacity style={styles.inputStyle} onPress={() => changeModalVisibility(true)}>
        <Text style={styles.labelSelect}>{chooseData}</Text>
        <View style={styles.iconContainer}>
          <Image source={require("../assets/seleccionar.png")} style={styles.iconSelect} />
        </View>
      </TouchableOpacity>
      <Modal transparent={true} animationType="fade"
             visible={isModalVisible} onRequestClose={() => changeModalVisibility(false)}>
        <ModalSelect changeModalVisibility={changeModalVisibility} setData={setData} marginTop={marginTop} />
      </Modal>
    </View>
  );
}

export default Select;

const styles = StyleSheet.create({
  iconContainer: {
    position: "absolute",
    right: 15,
    top: 15,
  },

  labelSelect: {
    color: "#fff",
    fontSize: 17,
    padding: 5,
    textAlign: "center",
  },

  iconSelect: {
    tintColor: "white",
    width: 20,
    height: 20,
  },

  containerInput: {
    margin: 10,
  },

  labelStyle: {
    marginBottom: 5,
    fontSize: 15,
  },

  inputStyle: {
    height: 50,
    backgroundColor: "#34495E",
    color: "#fff",
    fontSize: 17,
    padding: 8,
    borderRadius: 10,
  },
});
