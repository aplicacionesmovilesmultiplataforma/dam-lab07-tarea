import * as React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";

function TransferenceThird(props) {
  const { navigation } = props;
  return (
    <View style={styles.container}>
      <Image source={require("../assets/comprobar.png")} style={styles.imgOk} />
      <Text style={styles.labelOk}>Transferencia Satisfactoria</Text>
      <TouchableOpacity style={styles.btnOk} onPress={() => navigation.navigate("Home")}>
        <Text style={styles.labelBtn}>Aceptar</Text>
      </TouchableOpacity>
    </View>
  );
}

export default TransferenceThird;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  imgOk: {
    width: 200,
    height: 200,
  },

  labelOk: {
    padding: 10,
    fontSize: 20,
    color: "#0092ff",
    fontWeight: 'bold',
  },

  labelBtn: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
  },

  btnOk: {
    width: 200,
    height: 50,
    marginTop: 25,
    backgroundColor: "#34495E",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
});
