import React, { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image, Switch } from "react-native";
import Input from "./Input";
import DateTimePicker from "react-native-modal-datetime-picker";
import Select from "./Select";
import { useEffect } from "react";

const date = new Date();
const dateNow = `${date.getDate()}/${date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()}/${date.getFullYear()}`;

function TransferenceFirst({ navigation }) {
  const [chooseDataOrigen, setChooseDataOrigin] = useState("Seleccione una cuenta");
  const [chooseDataDestination, setChooseDataDestination] = useState("Seleccione una cuenta");
  const [amountText, setAmountText] = useState(0);
  const [referenceText, setReferenceText] = useState("");
  const [isVisibleDatePicker, setIsVisibleDatePicker] = useState(false);
  const [dateDisplay, setDateDisplay] = useState("");
  const [sendEmail, setSendEmail] = useState(false);
  const [novalid, setNovalid] = useState(true)

  useEffect(() => {
    setNovalid(validdationInput())
  }, [chooseDataDestination,chooseDataOrigen,amountText,referenceText]) 

  const validdationInput = () => {
    const validOrigen = chooseDataOrigen === 'Seleccione una cuenta'? false: chooseDataOrigen.length > 15? true: false
    const validDestination = chooseDataDestination === 'Seleccione una cuenta'? false: chooseDataOrigen.length > 15? true: false
    const validAmountText = isNaN(amountText) ? false : String(amountText).length >= 1?true:false
    const validReferenceText = referenceText.length > 1? true : false
    return  validOrigen?validDestination?validAmountText?validReferenceText?false:true:true:true:true
  }

  const setDataOrigen = (option) => setChooseDataOrigin(option);
  const setDataDestination = (option) => setChooseDataDestination(option);
  const showDatePicker = () => setIsVisibleDatePicker(true);
  const hideDatePicker = () => setIsVisibleDatePicker(false);
  const handleConfirm = (date) => {
    let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    let month = date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth();
    let datePicker = `${day}/${month}/${date.getFullYear()}`;
    setDateDisplay(datePicker);
    hideDatePicker();
  };
  const toggleSwitch = () => setSendEmail(previousState => !previousState);

  const onPressHandler = () => {
    navigation.navigate("Transference", {
      screen: "Transference",
      params: {
        rootAccount: chooseDataOrigen, destinationAccount: chooseDataDestination, amountText: amountText,
        referenceText: referenceText, dateDisplay: dateDisplay, sendEmail: sendEmail === false ? "NO" : "SI",
      },
    });
  };

  return (
    <View style={styles.container}>
      <Select text="Cuenta Origen" chooseData={chooseDataOrigen} setData={setDataOrigen} marginTop={155} />
      <Select text="Cuenta Destino" chooseData={chooseDataDestination} setData={setDataDestination} marginTop={250} />
      <Input text="Importe" value={amountText} onChangeText={ value => setAmountText( parseInt(value) )} editable={true} keyboardType="numeric" />
      <Input text="Referencia" value={referenceText} onChangeText={setReferenceText} editable={true}
              keyboardType="default" />
      <View style={styles.containerDs}>
        <TouchableOpacity onPress={showDatePicker}>
          <Image style={{ width: 35, height: 35 }} source={require("../assets/calendario.png")} />
        </TouchableOpacity>
        <DateTimePicker isVisible={isVisibleDatePicker} mode="date" onConfirm={handleConfirm}
                        onCancel={hideDatePicker} />
        <Text style={styles.labelDate}>{dateDisplay ? dateDisplay : setDateDisplay(dateNow)}</Text>
      </View>
      <View style={[styles.containerDs, { paddingTop: 7 }]}>
        <Text style={{ fontSize: 17 }}>Notificarme al correo</Text>
        <Switch
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={sendEmail ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch}
          value={sendEmail}
        />
      </View>
      <View style={styles.containerBtn}>
        <TouchableOpacity style={novalid?styles.btnDisabled:styles.btnNext} onPress={onPressHandler} disabled={novalid}>
          <Text style={styles.labelBtn}>Siguiente</Text>
        </TouchableOpacity>
        {
          novalid?
            (
              <View style={styles.containerAlert}>
                <Text style={styles.alertText}>Complete todos los campos</Text>
            </View>
            ): (<View></View>)
        }
      </View>
    </View>
  );
}

export default TransferenceFirst;

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingRight: 25,
  },

  labelDate: {
    fontSize: 17,
    marginLeft: 10,
    padding: 6,
    backgroundColor: "#d2d2d2",
    borderRadius: 10,
  },

  containerDs: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },

  containerBtn: {
    alignItems: "center",
    marginTop: 35,
  },

  labelBtn: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
  },

  btnNext: {
    height: 50,
    width: 200,
    backgroundColor: "#1f65ff",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  btnDisabled:{
    height: 50,
    width: 200,
    backgroundColor: "#D5D5D5",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  containerAlert:{
    marginTop:10,
    
  },
  alertText:{
    color:'#923939'
  }
});
