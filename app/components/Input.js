import * as React from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";

function Input(props) {
  const { text, value, onChangeText, editable, keyboardType } = props;
  return (
    <View style={styles.containerInput}>
      <Text style={styles.labelStyle}>{text}</Text>
      <TextInput style={styles.inputStyle} value={value} onChangeText={onChangeText} editable={editable}
                 keyboardType={keyboardType} />
    </View>
  );
}

export default Input;

const styles = StyleSheet.create({
  containerInput: {
    margin: 10,
  },

  labelStyle: {
    marginBottom: 5,
    fontSize: 15,
  },

  inputStyle: {
    height: 50,
    backgroundColor: "#34495E",
    color: "#fff",
    fontSize: 17,
    padding: 8,
    borderRadius: 10,
  },
});
